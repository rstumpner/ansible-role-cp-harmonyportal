## Ansible Role Sidecar
### vscode devcontainer
#### install a ready to use environment for manual testing
- install vscode
- start devcontainer
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install requests
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install docker
- execute molecule test