# Ansible Role for Checkpoint Harmony Portal Atomation 

This is a Ansible Role Checkpoint Harmony Portal Atomation. The Role is tested against a CI/CD Pipeline in Gitlab and have some Tags on the Tasks to Impelement some capabilities for a devops style deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-cp-harmonyportal
```
## Features
#### Build the Checkpoint Harmony Portal Configuration in JSON
Build the ACI Configuration in pure JSON to copy paste it to Postman (https://www.postman.com/) or VS Code Thunderclient (https://www.thunderclient.io/)

###### Step by Step Guide

###### Checkpoint Harmony Portal Features
- IDP Connection

 
License:
    MIT / BSD

Author Information:
roland@stumpner.at